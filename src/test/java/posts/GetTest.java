package posts;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.when;

public class GetTest {

        public final String URL = "http://localhost:3000/posts";
        @Test
        void shouldReturnCode200WhenGettingAllPosts() {
            when().get(URL).then().statusCode(200);
        }
        @Test
        void shouldReturnCode200WhenGettingExistingPost(){
            when().get(URL+"/2").then().statusCode(200);
        }
    }

