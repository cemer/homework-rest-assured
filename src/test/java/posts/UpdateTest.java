package posts;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class UpdateTest {
    String baseURL = "http://localhost:3000/posts";

    @Test
    void shouldReturnCode200WhenSavedNewPost() {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("title", "Proba Homework");
        postData.put("author", "Matt");
        Map<String, Object> updatedPostData = new HashMap<>(postData);
        updatedPostData.replace("author", "Mateusz");
        int id = given()

                .contentType(ContentType.JSON)
                .body(postData)
                .when()
                .post(baseURL)
                .then()
                .body("title",equalTo("Proba Homework"))
                .and()
                .body("author", equalTo("Matt"))
                .extract()
                .path("id");

        given()

                .contentType(ContentType.JSON)
                .body(updatedPostData)
                .when()
                .put(baseURL + "/" + id)
                .then()
                .body("title", equalTo("Proba Homework"))
                .and()
                .body("author", equalTo("Mateusz"))
                .statusCode(200);
    }
}
