package posts;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;

public class DeleteTest {

    String baseURL = "http://localhost:3000/posts";

    @Test
    void deleteExistingPost() {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("title", "Wakacje 2020 - relacja");
        postData.put("author", "AndrzejZ");

        int newPostId = given()

                .contentType(ContentType.JSON)
                .body(postData)
                .when()
                .post(baseURL)
                .body().jsonPath().get("id");

        when().delete(baseURL + "/"+ newPostId)
                .then().statusCode(200);
    }
}
