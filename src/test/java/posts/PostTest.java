package posts;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class PostTest {

    public final String URL = "http://localhost:3000/posts";

    @Test
        void shouldReturnCode200WhenSavedNewPost () {
        Map<String, Object> postData = new HashMap<String, Object>();
        postData.put("title", "Czerwiec - relacja");
        postData.put("author", "Matt");
        given()

                .contentType(ContentType.JSON)
                .body(postData)
                .when()
                .post(URL)
                .then()
                .statusCode(201);

    }

        }

